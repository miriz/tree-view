export interface Group {
    _groupId: number,
    _name: string,
    _systemId: number,
    _visible: number,
    EntityTypes?: any[]
}
export interface EntityType {
    id: number,
    name: string,
    iconId: number,
    Icon: Icon,
    _entityTypeId:number

}
export interface Icon {
    _iconId: number,
    _iconName: string

}



export interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
    id: number,
     iconId: number,
    Icon: Icon,
    _entityTypeId:number,
    _groupId
}