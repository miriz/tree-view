import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatToolbarModule, MatIconModule, MatGridListModule, MatTreeModule, MatFormFieldModule, MatInputModule, MatDividerModule, MatSelectModule,
} from '@angular/material'



@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatGridListModule,
    MatTreeModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatSelectModule

  ],
  exports: [
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatGridListModule,
    MatTreeModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatSelectModule





  ]
})
export class MaterialModule { }
