import { Component, OnInit } from '@angular/core';
import { SystemService } from 'src/app/services/system.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  systems: number[];
  selectedDevice;
  selectedSystem;
  constructor(public systemService: SystemService, private dataService: DataService) { }

  ngOnInit() {
    this.dataService.get('system').subscribe((res) => {
      this.systems = res;
      //Set default system
      this.selectedSystem = this.systems[0];
      this.systemService.setSystemId(this.selectedSystem);
    });
  }
  onChange(value) {
    this.selectedSystem = value;
    this.systemService.setSystemId(this.selectedSystem);
  }

}
