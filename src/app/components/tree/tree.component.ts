import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { Component, Input } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener, MatTreeNestedDataSource } from '@angular/material/tree';
import { DataService } from '../../services/data.service';
import { ExampleFlatNode, Group, EntityType } from '../../models/interfaces';
import { SystemService } from 'src/app/services/system.service';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent {
  public currentGroup: Group;
  public selectedEntity: any;
  @Input() systemId: any = 1;
  private transformer = (node: any, level: number) => {
    return {
      expandable: !!node.EntityTypes && node.EntityTypes.length > 0,
      name: node._name,
      level: level,
      id: node.Id,
      _entityTypeId: node._entityTypeId,
      iconId: node._iconId,
      Icon: node.Icon,
      _groupId: node._groupId

    };
  }
  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);
  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.EntityTypes);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  sysId: number;

  constructor(private dataService: DataService, private systemService: SystemService) {
    this.getData();
  }

  getData() {
    this.systemService.systemId.subscribe(
      (systemId) => {
        this.sysId = systemId
        this.selectedEntity = null;
        this.dataService.getSingle('system', systemId).subscribe((res) => {
          this.dataSource.data = res;
        })
      }
    );
  }
  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
  getEntity(node: any) {
    let index = this.treeControl.dataNodes.findIndex(n => n == node);
    let groupId;
    let groupName;

    for (let i = index; i >= 0; i--) {
      const element = this.treeControl.dataNodes[i];
      if (element._groupId) {
        groupId = element._groupId
        groupName = element.name;

        break;
      }
    }
    this.currentGroup = {
      _name: groupName,
      _systemId: this.sysId,
      _groupId: groupId,
      _visible: null,

    }


    this.dataService.getEntity('entity?systemId=' + this.sysId + '&groupId=' + groupId + '&id=' + node._entityTypeId).subscribe((res) => {
      this.selectedEntity = res;
    })
  }
  updateTree() {
    this.systemService.setSystemId(this.sysId);
    this.selectedEntity = null;
  }
}
