import { Component, OnInit } from '@angular/core';
import { SystemService } from 'src/app/services/system.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  tiles: any[] = [
    { name: 'tree', cols: 1, rows: 1, color: 'white' },
    { name: 'details', cols: 1, rows: 1, color: 'lightgray' },
  ];
  
  constructor() { 
  
  }

  ngOnInit() {
   
  }
  

}
