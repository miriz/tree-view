import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Group } from 'src/app/models/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit {
  @Input() selectedEntity: any;
  @Input() currentGroup: Group;
  @Output() updateTree = new EventEmitter();

  dataService: DataService;
  constructor(dataServices: DataService) {
    this.dataService = dataServices;
  }

  ngOnInit() {


  }
  updateEntity() {
    this.dataService.updateEntity('entity', {
      SystemId: this.currentGroup._systemId,
      GroupId: this.currentGroup._groupId,
      EntityId: this.selectedEntity._entityTypeId,
      Name: this.selectedEntity._name
    }).subscribe((res) => {
      this.updateTree.emit();
    })
  }

}
