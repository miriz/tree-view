import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private options = { headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*') };

  private baseUrl:string;
  constructor(private http: HttpClient) {
    
    
   }


  get(route): Observable<any> {
    return this.http.get(environment.baseUrl + route)
  }
  getSingle(route, id): Observable<any> {
    return this.http.get(environment.baseUrl + route + '/' + id)
  }
  getEntity(route): Observable<any> {
    return this.http.get(environment.baseUrl + route)
  }
  updateEntity(route, data): Observable<any> {
    return this.http.put(environment.baseUrl + route,data)
  }
}
