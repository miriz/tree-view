import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SystemService {
  public systemId = new Subject<number>();
 
  setSystemId(value: number) {
    this.systemId.next(value); 
  }
}
